<!doctype html>
<html class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <title>
    <?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' :'; } ?>
    <?php bloginfo( 'name' ); ?></title><!-- dns prefetch -->
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!-- css + javascript -->
    <?php wp_head(); ?>
    <script>
!function(){
        // configure legacy, retina, touch requirements @ conditionizr.com
        conditionizr()
    }()
    </script>
    <script type="text/javascript">
    var CCM_DISPATCHER_FILENAME = '/index.php';
        var CCM_CID = 1;
        var CCM_EDIT_MODE = false;
        var CCM_ARRANGE_MODE = false;
        var CCM_IMAGE_PATH = "/concrete/images";
        var CCM_TOOLS_PATH = "/index.php/tools/required";
        var CCM_BASE_URL = "http://kiewit.com";
        var CCM_REL = "";
    </script>
    <script src=
    "<?php echo get_template_directory_uri(); ?>/scripts/jquery.js"
    type="text/javascript"></script><!-- build:js scripts/ccm.base.min.js -->

    <script src=
    "<?php echo get_template_directory_uri(); ?>/scripts/ccm.base.js"></script><!-- endbuild -->
    <!-- build:js scripts/jqueryconfig.min.js -->

    <script src=
    "<?php echo get_template_directory_uri(); ?>/scripts/jqueryconfig.js"></script><!-- endbuild -->
    <!-- build:js scripts/jquery.modal.min.js -->

    <script src=
    "<?php echo get_template_directory_uri(); ?>/scripts/jquery.modal.min.js"></script><!-- endbuild -->
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="css/IE8.css" />
    <![endif]-->
</head>

<body>
    <div class="mobileonly nav_mobilesidebin" id="menu">
        <div class=" ccm-block-styles" id="blockStyle4719Main212">
            <?php wpeSideNav(); ?>
        </div>

        <?php wpeFootNav(); ?>
    </div>


    <div id="right">
        <div id="maxcontainerbin">
            <div id="header-image-interior" style="background-image: url(src="<?php echo get_template_directory_uri(); ?>/images/Mining_Header2.jpg); background-size: cover; background-repeat: no-repeat;">
                <div class="ccm-random-image"><img alt="Mining_Header2.jpg" height="225" src="<?php echo get_template_directory_uri(); ?>/images/Mining_Header2.jpg" width="1380"></div>
            </div>


            <div id="billboardoverlayinterior" style=
            "background:url(src="<?php echo get_template_directory_uri(); ?>/images/interiorheaderimg_overlay.png); background-repeat:repeat;">

            </div>


            <div id="headerbin" class="clearfix">
                <div id="logobin">
                    <a href="/"><img alt=
                    "Kiewit" border="0" height="72" src=
                    "<?php echo get_template_directory_uri(); ?>/images/kiewitlogo_2x.jpg"
                    title="Kiewit" width="179"></a>
                </div>


                <div class="clear mobileonly">
                </div>


                <div id="columndivider"><img alt="" border="0" height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="21">
                </div>


                <div class="nomobile clearfix" id="navbarbin">
                    <?php wpeHeadNav(); ?>
                </div>
                <!-- navbarbin -->


                <div class="nomobile notablet" id="searchbin">
                    <form action="http://kiewit.com/index.php/search/" method=
                    "get">
                        <div id="searchbin">
                            <div id="searchfieldbin">
                                <input class="searchfield formfieldswap" name=
                                "query" type="text" value="search...">

                                <div class="closesearchbtn graphicmo"><img alt=
                                "Hide Search Field" border="0" height="12" src="<?php echo get_template_directory_uri(); ?>/images/closebtn.png"
                                title="Hide Search Field" width="12">
                                </div>
                            </div>
                            <input class="searchbutton" height="17" name=
                            "submit" src="<?php echo get_template_directory_uri(); ?>/images/search_icon.png"
                            type="image" width="21"> <input name=
                            "search_paths[]" type="hidden" value="">
                        </div>
                    </form>


                    <div class="opensearchbtn"><img alt="Show Search Field"
                    border="0" height="50" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" title=
                    "Show Search Field" width="50">
                    </div>
                </div>

            </div><!-- #headerbin -->


            <div class="clear"></div>

            <div><img alt="" border="0" height="45" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="21">
            </div>

            <div class="interiorpageheaderbin clearfix">
                <div class="interiorpageheaderinner binw6">

                    <div class="interiorpageheadline"><h1>
                        <? if ( is_single() ){  ?>
                            <?php the_title(); ?>
                        <? } elseif ( is_page()){ ?>
                            <?php the_title(); ?>
                        <? } elseif ( is_category()){ ?>
                            <?php the_category(', '); ?>
                        <?  } else { ?>
                            <!-- nothing -->
                        <?  }  ?>
                        </h1>
                    </div>

                    <div id="breadcrumbbin">
                        <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                    </div>
                </div>
                <div class="interiorpagequote"></div>
            </div>

            <div id="spacerbin"><img alt="" border="0" height="23" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2">
            </div>
            <div class="mobileonly" style="height:30px;"><img alt="" border="0"
                height="23" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2">
            </div>