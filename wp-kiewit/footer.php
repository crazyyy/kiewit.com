<div id="spacerbin"><img alt="" border="0" height="74" src=
    "<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2">
</div>
<div id="footerbin">
    <div id="spacerbin"><img alt="" border="0" height="30" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
        width="2">
    </div>
    <div id="contentbin">
        <div class="mobileonly">
            <div class="accordionheading" style=
                "text-align:center; border-bottom:1px solid #ffce00 !important;">Quick Links
            </div>
            <div class="accordioninfo clearfix" style=
                " border-bottom:1px solid #ffce00;">
                <?php wpeFootLFNav(); ?>
                <?php wpeFooRGFNav(); ?>
            </div>
            <div class="accordionheading" style=
                "text-align:center; border-bottom:1px solid #ffce00 !important;">
                Featured Projects
            </div>
            <div class="accordioninfo" style=
                " border-bottom:1px solid #ffce00; text-align:center !important;">
                <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Interior Footer" -->
                <?php query_posts("showposts=2&cat=1"); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="featuredprojectinteriorfooter">
                    <div class="featuredprojectimg">
                        <!-- post thumbnail -->
                        <a rel="nofollow" class="feature-img" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php if ( has_post_thumbnail()) :
                            the_post_thumbnail('medium');
                            else: ?>
                            <img src="<?php echo catchFirstImage(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
                        <?php endif; ?></a>
                        <!-- /post thumbnail -->
                    </div>
                    <!-- end featuredprojectimg -->
                    <div id="columndivider">
                    <img alt="" border="0" height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="12"></div>
                    <div class=
                        "featuredprojectinteriorfooterleftbin">
                        <div class="featuredprojectname">
                            <?php the_title(); ?>
                        </div>
                        <!-- end featuredprojectname -->
                        <div class="featuredprojectlocation">
                        </div>
                        <!-- end featuredprojectlocation -->
                        <div class="featuredprojectlink">
                            <a href="<?php the_permalink(); ?>">
                            Read More</a>
                        </div>
                        <!-- end featuredprojectlink -->
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <!-- end featuredprojectinteriorfooter -->
                <div id="spacerbin"><img alt="" border="0" height="24" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1"></div>
                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
            </div>
            <div class="accordionheading" style=
                "text-align:center; border-bottom:1px solid #ffce00 !important;">
                Contact Kiewit
            </div>
            <div class="accordioninfo" style=
                " border-bottom:1px solid #ffce00; text-align:center !important; color:#fff;">
                <h4 style="text-align: center;">Home Office : 3555
                Farnam St. Omaha, NE</h4>
                <h4 style="text-align: center;">Questions:<a class="textlinkwhite" href="#" style="font-size: 14px; line-height: 1.5;">Contact Us</a></h4>
                <p style="text-align: center;"><a class=
                    "textlinkwhite" href=
                    "http://kiewit.com/!trash/locations-2/" title=
                "Locations">Locations Page</a>   |
                  <a class="textlinkwhite" href=
                    "http://kiewit.com/" title="Careers">Career
                Search</a></p>
            </div>
        </div>
        
        <div class="binw2 nomobile clearfix" id="columnbin">
            <h3>Quick Links</h3>
            <div class="tnSpacer" style="height:14px">
            </div>
            <div class="footerquicklinksone ccm-block-styles" id="blockStyle2841Main24">
                <?php wpeFootLFNav(); ?>
            </div>
            <div class="footerquicklinkstwo ccm-block-styles" id="blockStyle114Main4">
                <?php wpeFooRGFNav(); ?>
            </div>
        </div>
        <div class="nomobile" id="columndivider"><img alt="" border="0" height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="20"></div>
        <!-- Featured Projects -->
        <div class="binw3 nophone notabletportrait clearfix" id="columnbin">
            <h3>Featured Projects</h3>
            <div class="tnSpacer" style="height:19px">
            </div>
            <?php query_posts("showposts=2&cat=1"); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="featuredprojectinteriorfooter">
                <div class="featuredprojectimg">
                    <!-- post thumbnail -->
                    <a rel="nofollow" class="feature-img" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php if ( has_post_thumbnail()) :
                                the_post_thumbnail('medium');
                        else: ?>
                        <img src="<?php echo catchFirstImage(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
                    <?php endif; ?></a>
                    <!-- /post thumbnail -->
                </div>
                <!-- end featuredprojectimg -->
                <div id="columndivider"><img alt="" border="0"
                    height="2" src=
                    "<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                    width="12">
                </div>
                <div class="featuredprojectinteriorfooterleftbin">
                    <div class="featuredprojectname">
                        <?php the_title(); ?>
                    </div>
                    <div class="featuredprojectlocation">
                    </div>
                    <div class="featuredprojectlink">
                        <a href="<?php the_permalink(); ?>">
                        Read More</a>
                    </div>
                    <!-- end featuredprojectlink -->
                </div>
            </div>
            <!-- end featuredprojectinteriorfooter -->
            <div id="spacerbin">
                <img alt="" border="0" height="24" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1">
            </div>
            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            </div><!-- Featured Projects -->
            <div class="nomobile" id="columndivider"><img alt=""
                border="0" height="2" src=
                "<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                width="20">
            </div>
            <div class="binw4 nophone notabletportrait" id="columnbin">
                <h3>Contact Kiewit</h3>
                <div class="tnSpacer" style="height:14px">
                </div>
                <h4 style="float: left;">Home Office :</h4>
                <p style="float: left; margin-bottom: -15px;">
                <span> 3555 Farnam St. Omaha, NE<br>
                 </span>
                </p>
                <div class="clear"></div>
                <p><a class="textlinkwhite" href=
                    "http://kiewit.com/locations/" title=
                "Locations">Locations Page</a>   |
                  <a class="textlinkwhite" href=
                    "http://www.kiewitjobs.com/" title="Careers">Career
                Search</a></p>
                <div id="footerhomesocialmediabin">
                    <div><img alt="" border="0" height="6" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="1">
                    </div>
                    <div class="homepagesocial" id="socialmail">
                        <a href="#"></a>
                    </div>
                    <div id="columndivider"><img alt="" border="0"
                        height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="13">
                    </div>
                    <div class="homepagesocial" id="socialfacebook">
                        <a href="#" target="_facebook"></a>
                    </div>
                    <div id="columndivider"><img alt="" border="0"
                        height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="13">
                    </div>
                    <div class="homepagesocial" id="socialtwitter">
                        <a href="#" target=
                        "_twitter"></a>
                    </div>
                    <div id="columndivider"><img alt="" border="0"
                        height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="13">
                    </div>
                    <div class="homepagesocial" id="sociallinkedin">
                        <a href="#"
                        target="_linkedin"></a>
                    </div>
                    <div id="columndivider"><img alt="" border="0"
                        height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="13">
                    </div>
                    <div class="homepagesocial" id="socialyoutube">
                        <a href="#"
                        target="youtube"></a>
                    </div>
                    <div id="columndivider"><img alt="" border="0"
                        height="2" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif"
                        width="17">
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div id="spacerbin"><img alt="" border="0" height="15" src=
            "<?php echo get_template_directory_uri(); ?>/images/fill.gif"
            width="2">
        </div>
        <div id="footerinteriorlinksbin">
            <div id="footerinteriorlinksinner" class="clearfix">
                <div id="footerhomecopyrightbin">
                    © 2014 Kiewit Corporation. All Rights Reserved.
                </div>
                <div class="nophone notabletportrait">
                    <?php wpeFootNav(); ?>
                </div>
                <div id="tothetoplink">
                    <a class="top-link" href="#top"><img alt="Top" border="0" height="12" src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" title="Top" width="35"></a>
                </div>
            </div>
            </div><!-- #footerinteriorlinksbin -->
        </div>
    </div>
    <!-- end maxcontainerbin -->
    <div class="mobileonly graphicmo2" id="button"><img alt="Menu" border=
        "0" height="22" src="<?php echo get_template_directory_uri(); ?>/images/menu_icon.png"
        title="Menu" width="22">
    </div>
</div>
</body>
</html>