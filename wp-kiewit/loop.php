<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	


<div class="plp_leftsidebar">
	<div class="projectlanding_infobin">
		<div class="project-description" id="project-description-img">
			<!-- post thumbnail -->
			<a rel="nofollow" class="feature-img" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php if ( has_post_thumbnail()) :
					the_post_thumbnail('medium');	
				else: ?>
				<img src="<?php echo catchFirstImage(); ?>" class="ccm-output-thumbnail" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
			<?php endif; ?></a>
			<!-- /post thumbnail -->
		</div>
		<div class="ccm-page-list-description"></div>
        <div class="project-description clearfix">
            <div class="project-desc-scroll">
            	<h3 class="ccm-page-list-title"><a href="<?php the_permalink(); ?>" target="_self"><?php the_title(); ?></a></h3> 
                <h5 class="clr_ltgray"> </h5>
				<?php wpeExcerpt('wpeExcerpt20'); ?>
				<div>
					<a class="yellowbutton_new" href="<?php the_permalink(); ?>" target="_self">Read More</a>
				</div>
            </div><!-- project-desc-scroll -->
        </div>
    </div>
</div>
	
<?php endwhile; else: ?>

	<!-- article -->
	<article>
		<h2 class="title"><?php _e( 'Sorry, nothing to display.', 'wpeasy' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
