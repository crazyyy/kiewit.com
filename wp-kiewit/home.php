<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' :'; } ?> <?php bloginfo( 'name' ); ?></title>

    <!-- dns prefetch -->
    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- css + javascript -->
    <?php wp_head(); ?>
    <script>
    !function(){
        // configure legacy, retina, touch requirements @ conditionizr.com
        conditionizr()
    }()
    </script>

    <script type="text/javascript">
        var CCM_DISPATCHER_FILENAME = '/index.php';
        var CCM_CID = 1;
        var CCM_EDIT_MODE = false;
        var CCM_ARRANGE_MODE = false;
        var CCM_IMAGE_PATH = "/concrete/images";
        var CCM_TOOLS_PATH = "/index.php/tools/required";
        var CCM_BASE_URL = "http://kiewit.com";
        var CCM_REL = "";
    </script>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.js"></script>

    <!-- build:js scripts/ccm.base.min.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/ccm.base.js"></script>
    <!-- endbuild -->
    <!-- build:js scripts/jqueryconfig.min.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/jqueryconfig.js"></script>
    <!-- endbuild -->
    <!-- build:js scripts/jquery.modal.min.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.modal.min.js"></script>
    <!-- endbuild -->

    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="css/IE8.css" />
    <![endif]-->

</head>
<body <?php body_class(); ?>>
    <div id="menu" class="mobileonly nav_mobilesidebin">
        
        <div id="blockStyle4719Main212" class="ccm-block-styles">
            <?php wpeSideNav(); ?>
        </div>

        <ul class="ccm-manual-nav">
            <li class="">
                <a href="http://kiewit.com/info/privacy-policy/" class="">
            Privacy Policy      </a>
            </li>


            <li class="">
                <a href="http://kiewit.com/info/terms-conditions/" class="">
            Terms &amp; Conditions      </a>
            </li>


            <li class="">
                <a href="http://kiewit.com/business-with-us/" class="">
            Business With Us        </a>
            </li>


            <li class="">
                <a href="http://kiewit.com/info/sitemap/" class="">
            Sitemap     </a>
            </li>


            <li class="">
                <a href="http://kiewit.com/search/" class="">
            Search      </a>
            </li>

        </ul>

    </div>

    <div id="right">

        <div id="homepageawards" style="display: none;">
            <ul>
                <li><img src="<?php echo get_template_directory_uri(); ?>/images/fortune-100-best-companies-to-work-for-2014.jpg" alt="fortune-100-best-companies-to-work-for-2014.jpg" width="150" height="150">
                </li>
                <li><img src="<?php echo get_template_directory_uri(); ?>/images/fortunes-worlds-most-admired-companies-2014.jpg" alt="fortunes-worlds-most-admired-companies-2014.jpg" width="150" height="150">
                </li>
                <li><img src="<?php echo get_template_directory_uri(); ?>/images/great-place-to-work-canada-2014.png" alt="great-place-to-work-canada-2014.png" width="150" height="150">
                </li>
            </ul>
        </div>

        <div id="header-image" style="opacity: 1; background: url(<?php echo get_template_directory_uri(); ?>/images/HomeBackgroundImage1.jpg)  no-repeat rgb(51, 51, 51);"></div>

        <div id="billboardoverlay" style="background:url("<?php echo get_template_directory_uri(); ?>/images/homepagebillboard_overlay.png"); background-repeat:repeat;"></div>




        <div id="maxcontainerbin" style="background-color:transparent;">
            <div id="headerbin">
                <div id="logobin">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/kiewitlogo_2x.jpg" width="179" height="72" alt="Kiewit" title="Kiewit" border="0">
                    </a>
                </div>
                <div class="clear mobileonly"></div>
                <div id="columndivider">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="21" height="2" alt="" border="0">
                </div>

                <div id="navbarbin" class="nomobile">
                    <?php wpeHeadNav(); ?>
                    <div class="clear"></div>
                </div><!-- navbarbin -->

                <div id="searchbin" class="notablet nomobile">
                    <form action="http://kiewit.com/index.php/search/" method="get">
                        <div id="searchbin">
                            <div id="searchfieldbin">
                                <input name="query" type="text" value="search..." class="searchfield formfieldswap">
                                <div class="closesearchbtn graphicmo">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/closebtn.png" width="12" height="12" alt="Hide Search Field" title="Hide Search Field" border="0">
                                </div>
                            </div>
                            <input name="submit" type="image" src="<?php echo get_template_directory_uri(); ?>/images/search_icon.png" width="21" height="17" border="0" class="searchbutton">
                            <input name="search_paths[]" type="hidden" value="">
                        </div>
                    </form>
                    <div class="opensearchbtn"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="50" height="50" alt="Show Search Field" title="Show Search Field" border="0">
                    </div>
                </div>
                <div class="clear"></div>
            </div><!-- headerbin -->
            
            <div class="nomobile">

                <script>
                $.modal.defaults = {
                    overlay: "#000", // Overlay color
                    opacity: 0.75, // Overlay opacity
                    zIndex: 90, // Overlay z-index.
                    escapeClose: true, // Allows the user to close the modal by pressing `ESC`
                    clickClose: true, // Allows the user to close the modal by clicking the overlay
                    closeText: 'Close', // Text content for the close <a> tag.
                    showClose: false, // Shows a (X) icon/link in the top-right corner
                    modalClass: "modal", // CSS class added to the element being displayed in the modal.
                    spinnerHtml: null, // HTML appended to the default spinner during AJAX requests.
                    showSpinner: true, // Enable/disable the default spinner during AJAX requests.
                    fadeDuration: null, // Number of milliseconds the fade transition takes (null means no transition)
                    fadeDelay: 1.0 // Point during the overlay's fade-in that the modal begins to fade in (.5 = 50%, 1.5 = 150%, etc.)
                };

                $(document).ready(function() {
                    var mode = 'horizontal';
                    var speed = 7000;
                    var infiniteLoop = true;
                    var pager = false;
                    var controls = true;


                    var url1 = $('.igc_billboard_item:first').attr('billboardbackground');
                    var bgString1 = " url('" + url1 + "')";
                    $('#header-image').css({
                        'background-image': bgString1,
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover'
                    });
                    $('#header-image').css("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + url1 + "', sizingMethod='scale')");
                    $('#header-image').css("-ms-filter", "\"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + url1 + "',sizingMethod='scale')\";");
                    $('.igc_billboard_item:first').show();

                    var timeoutID;

                    function billboardTimer() {
                        timeoutID = window.setTimeout(changeBillboard, speed);
                    }

                    function changeBillboard(offset) {
                        offset = offset || 1

                        clearbBillboardTimer();

                        var $selected = $('.igc_bb_shown');
                        var $all = $('#igc_billboard .igc_billboard_item');

                        var length = $all.length;
                        var index = $all.index($selected);
                        var next = (index + offset) % length;
                        var $next = $all.eq(next);


                        $selected.removeClass('igc_bb_shown');
                        $selected.hide();

                        $next.fadeIn(1250);
                        $next.addClass('igc_bb_shown');

                        //SHOW HOME PAGE AWARDS ON FIRST BILLBOARD ONLY
                        if (next == 0) {
                            $('#homepageawards').show();
                        } else {
                            $('#homepageawards').hide();
                        }
                        //END HOME PAGE AWARDS	

                        $origImage = $('#header-image');
                        $newImage = $origImage.clone().insertBefore($origImage);

                        var url = $('.igc_bb_shown').attr('billboardbackground');
                        var bgString = " url('" + url + "')";
                        $newImage.css({
                            'background-image': bgString,
                            'background-repeat': 'no-repeat',
                            'background-size': 'cover'
                        }).fadeTo(750, 1);
                        $newImage.css("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + url + "', sizingMethod='scale')");
                        $newImage.css("-ms-filter", "\"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + url + "',sizingMethod='scale')\";");

                        $origImage.fadeTo(750, 0, function() {
                            $(this).remove();
                        });

                        billboardTimer();
                    }

                    $('a[rel="modal:custom"]').click(function(e) {
                        $("#modal-wrapper").modal();

                        var $caller = $(e.target);
                        var url = $caller.attr('href');

                        var $modalContent = $("#modal-content").empty();
                        var loadDiv = url + " .homepagepopupbin"

                        $modalContent.load(loadDiv);

                        $("#modal-nav a").removeClass('active');
                        $("#modal-nav a[href$='" + url + "']").addClass('active');

                        $('#modal-content').height($('#modal-wrapper').height());
                        $('#modal-nav').height($('#modal-wrapper').height());



                        e.stopPropagation();
                        return false;
                    });



                    function clearbBillboardTimer() {
                        window.clearTimeout(timeoutID);
                    }

                    billboardTimer();

                    $('.igc_billboard_item:first').addClass('igc_bb_shown');


                    $('#homepagearrowsleft').click(function(e) {
                        changeBillboard(-1);
                    });

                    $('#homepagearrowsright').click(function(e) {
                        changeBillboard();
                    });

                    $('.igc_billboard_item').mouseenter(function(e) {
                        clearbBillboardTimer();
                    });

                    $('.igc_billboard_item').mouseleave(function(e) {
                        clearbBillboardTimer();
                        billboardTimer();
                    });
                    $('#homepagearrowsright').mouseenter(function(e) {
                        clearbBillboardTimer();
                    });

                    $('#homepagearrowsright').mouseleave(function(e) {
                        clearbBillboardTimer();
                        billboardTimer();
                    });
                    $('#homepagearrowsleft').mouseenter(function(e) {
                        clearbBillboardTimer();
                    });

                    $('#homepagearrowsleft').mouseleave(function(e) {
                        clearbBillboardTimer();
                        billboardTimer();
                    });

                });
                </script>
            </div>

            
            <div class="homeslidepieces" id="igc_billboard">
                <div class="nomobile">
                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/Marsh_Landing.jpg" style="display: none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>A Great Place to Work</h2>
                                </div>
                                <!-- end igc_billboard_healine -->


                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Internal Link" -->
                                <div class="igc_billboard_internalsubhead">
                                    <h3><a href="http://www.kiewit.com/about-us/awards/">Company culture, strong leadership and Kiewit’s unparalleled commitment to development and wellness programs, are only part of the reason that Kiewit is recognized as a great place to work.</a></h3>
                                </div>
                                <!-- end igc_billboard_internalsubhead -->

                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item igc_bb_shown" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/HomeBackgroundImage1.jpg">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>KIEWAYS MAGAZINE — AN INSIDE LOOK AT KIEWIT’S WORK</h2>
                                </div>
                                <!-- end igc_billboard_healine -->


                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Internal Link" -->
                                <div class="igc_billboard_internalsubhead">
                                    <h3><a href="http://www.kiewit.com/media/kieways/">From roads and dams to high-rise buildings and power plants — learn about the latest Kiewit projects that shape our world.</a></h3>
                                </div>
                                <!-- end igc_billboard_internalsubhead -->

                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/iatanhome.jpg" style="display:none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>Nobody Gets Hurt.</h2>
                                </div>
                                <!-- end igc_billboard_healine -->


                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Internal Link" -->
                                <div class="igc_billboard_internalsubhead">
                                    <h3><a href="http://www.kiewit.com/our-commitment/safety/">At Kiewit, nothing is more important.</a></h3>
                                </div>
                                <!-- end igc_billboard_internalsubhead -->

                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/TD_Ameritrade_Omaha_Campus_Omaha-Ne_08.JPG" style="display:none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>People. Integrity. Excellence. Stewardship.</h2>
                                </div>
                                <!-- end igc_billboard_healine -->


                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Internal Link" -->
                                <div class="igc_billboard_internalsubhead">
                                    <h3><a href="http://www.kiewit.com/about-us/core-values/">These four core values are the foundation of everything we do. It’s who we are as a company.</a></h3>
                                </div>
                                <!-- end igc_billboard_internalsubhead -->

                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/sepulveda.jpg" style="display:none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>Experience matters.</h2>
                                </div>
                                <!-- end igc_billboard_healine -->


                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Internal Link" -->
                                <div class="igc_billboard_internalsubhead">
                                    <h3><a href="http://www.kiewit.com/markets/">In the past 10 years, Kiewit has completed more than 4,000 projects in the markets we serve.</a></h3>
                                </div>
                                <!-- end igc_billboard_internalsubhead -->

                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/cobra-castor.jpg" style="display:none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>Local presence. Vast, expansive reach.</h2>
                                </div>
                                <!-- end igc_billboard_healine -->

                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Text" -->
                                <div class="igc_billboard_textsubhead">
                                    <h3> Urban cities. Remote camp jobs. Deserts, tundras and jungles. With offices in North America and Australia, we have the resources necessary to go where the work is.</h3>
                                </div>
                                <!-- end igc_billboard_textsubhead -->


                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->






                    <div class="igc_billboard_item" billboardbackground="<?php echo get_template_directory_uri(); ?>/images/san-miguel.jpg" style="display:none;">
                        <div class="slidedetail">
                            <div class="slidedetail-inner">
                                <div class="igc_billboard_healine">
                                    <h2>We build it.</h2>
                                </div>
                                <!-- end igc_billboard_healine -->

                                <!-- ENTER MARKUP HERE FOR FIELD "Subhead Type" : CHOICE "Text" -->
                                <div class="igc_billboard_textsubhead">
                                    <h3> Our ability to self-perform our own work is a fundamental differentiator on many of our clients’ projects.</h3>
                                </div>
                                <!-- end igc_billboard_textsubhead -->


                            </div>
                            <!-- end slidedetail-inner -->
                        </div>
                        <!-- end slidedetail -->
                    </div>
                    <!-- end igc_billboard_item -->





                </div>
                <div class="clear"></div>
                <div id="homemarketslinkbin" class="nomobile notablet">
                    <div class="homemarketslink"><a id="homemarketsbuilding" href="http://kiewit.com/homepage-popups/building" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsmining" href="http://kiewit.com/homepage-popups/mining" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsoil" href="http://kiewit.com/homepage-popups/oil-gas-chemical" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketspower" href="http://kiewit.com/homepage-popups/power" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketstransportation" href="http://kiewit.com/homepage-popups/transportation" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketswater" href="http://kiewit.com/homepage-popups/waterwastewater" rel="modal:custom"><span></span></a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div id="homepagearrows">
                    <div id="homepagearrowsleft"></div>
                    <div id="homepagearrowsright"></div>
                    <div class="clear"></div>
                </div>
                <div id="homemarketslinkbin" class="tabletonly">
                    <div class="homemarketslink"><a id="homemarketsbuilding" href="http://kiewit.com/markets/building"><span>Building</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsmining" href="http://kiewit.com/markets/mining"><span>Mining</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsoil" href="http://kiewit.com/markets/oil-gas-chemical"><span>Oil</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketspower" href="http://kiewit.com/markets/power"><span>Power</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketstransportation" href="http://kiewit.com/markets/transportation"><span>Transportation</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketswater" href="http://kiewit.com/markets/waterwastewater"><span>Water</span></a>
                    </div>
                    <div class="clear"></div>
                </div>
                <div id="homemarketslinkbin" class="mobileonly">
                    <div class="homemarketslinkbin-hdr">OUR MARKETS</div>
                    <div class="clear"></div>
                    <div class="homemarketslink"><a id="homemarketsbuilding" href="http://kiewit.com/markets/building"><span>Building</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsmining" href="http://kiewit.com/markets/mining"><span>Mining</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketsoil" href="http://kiewit.com/markets/oil-gas-chemical"><span>Oil</span></a>
                    </div>
                    <div class="clear"></div>
                    <div class="homemarketslink"><a id="homemarketspower" href="http://kiewit.com/markets/power"><span>Power</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketstransportation" href="http://kiewit.com/markets/transportation"><span>Transportation</span></a>
                    </div>
                    <div class="homemarketslink"><a id="homemarketswater" href="http://kiewit.com/markets/waterwastewater"><span>Water</span></a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>



            <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="30" height="2" alt="" border="0">
            </div>
            <div class="clear"></div>

            <div id="modal-wrapper" class="modal" style="position: fixed; top: 50%; left: 50%; margin-top: -350px; margin-left: -490px; z-index: 91; display: none; background: rgb(34, 34, 34) !important;">
                <div id="modal-nav" style="height: 700px;">
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/building" rel="modal:custom" id="homemarketsbuildingpu" class="active"></a>
                    </div>
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/mining" rel="modal:custom" id="homemarketsminingpu"></a>
                    </div>
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/oil-gas-chemical" rel="modal:custom" id="homemarketsoilpu"></a>
                    </div>
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/power" rel="modal:custom" id="homemarketspowerpu"></a>
                    </div>
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/transportation" rel="modal:custom" id="homemarketstransportationpu"></a>
                    </div>
                    <div class="modal-button">
                        <a href="http://kiewit.com/homepage-popups/waterwastewater" rel="modal:custom" id="homemarketswaterpu"></a>
                    </div>
                </div>

                <div class="content-wrapper">
                    <div id="modal-content" style="height: 700px;">
                        <div class="homepagepopupbin">
                            <div id="hppbcontentbin">
                                <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2" height="30" alt="" border="0">
                                </div>
                                <div id="columnbin" class="binw0a homepagepopuptop">
                                    <div style="text-align:center;"><img border="0" class="ccm-image-block" alt="" src="<?php echo get_template_directory_uri(); ?>/images/horizontal-building-yellow.png" width="159" height="50">
                                    </div>
                                    <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2" height="35" alt="" border="0">
                                    </div>
                                    <p>Kiewit specializes in the construction of office buildings; industrial complexes; education and sports facilities; hotels and hospitals; transportation terminals; science and technology facilities; manufacturing, retail and special-use facilities; and extensive interior construction with tenant improvements. Our capabilities include general construction, construction management, design-build, design-assist and turn-key project development. We also provide fast, accurate preconstruction project management services. Over the last 10 years, Kiewit has performed construction services for more than 1,200 vertical construction projects totaling more than $7.2 billion in revenue.</p>
                                </div>
                                <div class="clear"></div>
                                <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="2" height="35" alt="" border="0">
                                </div>
                                <div id="columnbin" class="binw1a">
                                    <h2 class="clr_white">Featured Project</h2>
                                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Homepage Popup Main" -->
                                    <div class="featuredprojecthomepagepopupmain">
                                        <div class="featuredprojectimg"><img src="<?php echo get_template_directory_uri(); ?>/images/25629f95aeddfbd3c3c688515eefc28a_f80.JPG" width="416" alt="TD Ameritrade Headquarters" border="0">
                                        </div>
                                        <!-- end featuredprojectimg -->

                                        <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="23" alt="" border="0">
                                        </div>

                                        <div class="featuredprojectnamelocation">
                                            TD Ameritrade Headquarters
                                            <!-- end featuredprojectname -->- Omaha, Neb.
                                            <!-- end featuredprojectlocation -->
                                        </div>

                                        <div class="featuredprojectlink">
                                            <a href="http://www.kiewit.com/projects/building/commercial/td-ameritrade-headquarters/">Read More</a>
                                        </div>
                                        <!-- end featuredprojectlink -->
                                        <div class="clear"></div>
                                    </div>
                                    <!-- end featuredprojecthomepagepopupmain -->










                                </div>
                                <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="33" height="2" alt="" border="0">
                                </div>
                                <div id="columnbin" class="binw1c">
                                    <h2 class="clr_white">Other Projects</h2>


                                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Homepage Popup Other" -->
                                    <div class="featuredprojecthomepagepopupother">
                                        <div class="featuredprojectimg">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/11f82a804e597763626d99b106883dad_f81.jpg" width="112" alt="Blue Cross Centre" border="0">
                                        </div>
                                        <!-- end featuredprojectimg -->
                                        <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="10" height="2" alt="" border="0">
                                        </div>
                                        <div class="featuredprojecthomepagepopupotherleftbin">
                                            <div class="featuredprojectnamelocation">
                                                Blue Cross Centre
                                                <!-- end featuredprojectname -->
                                                - Omaha, Neb.
                                                <!-- end featuredprojectlocation -->
                                            </div>
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectdescriptionhomeother">The BlueCross Centre is a LEED Silver certified 316,000-square foot building built to combine company ope...</div>
                                            <!-- end featuredprojectdescription -->
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectlink">
                                                <a href="http://www.kiewit.com/projects/building/sports-entertainment/td-ameritrade-park/">Read More</a>
                                            </div>
                                            <!-- end featuredprojectlink -->
                                        </div>
                                        <div class="clear"></div>
                                        <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="21" alt="" border="0">
                                        </div>
                                    </div>
                                    <!-- end featuredprojecthomepagepopupother -->










                                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Homepage Popup Other" -->
                                    <div class="featuredprojecthomepagepopupother">
                                        <div class="featuredprojectimg">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/e3f9a23e2218fd2d856219c6f344041b_f122.jpg" width="112" alt="Trump International Hotel" border="0">
                                        </div>
                                        <!-- end featuredprojectimg -->
                                        <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="10" height="2" alt="" border="0">
                                        </div>
                                        <div class="featuredprojecthomepagepopupotherleftbin">
                                            <div class="featuredprojectnamelocation">
                                                Trump International Hotel
                                                <!-- end featuredprojectname -->
                                                - Waikiki, Hawaii
                                                <!-- end featuredprojectlocation -->
                                            </div>
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectdescriptionhomeother">The Trump International Hotel is a 350-foot tower with approximately 460 luxury hotel condominium units r...</div>
                                            <!-- end featuredprojectdescription -->
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectlink">
                                                <a href="http://www.kiewit.com/projects/building/hospitality/trump-international-hotel/">View More</a>
                                            </div>
                                            <!-- end featuredprojectlink -->
                                        </div>
                                        <div class="clear"></div>
                                        <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="21" alt="" border="0">
                                        </div>
                                    </div>
                                    <!-- end featuredprojecthomepagepopupother -->










                                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Homepage Popup Other" -->
                                    <div class="featuredprojecthomepagepopupother">
                                        <div class="featuredprojectimg">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/5194cb0eb05900a5d3576165044e094a_f129.jpg" width="112" alt="CU Boulder Kittredge Loop" border="0">
                                        </div>
                                        <!-- end featuredprojectimg -->
                                        <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="10" height="2" alt="" border="0">
                                        </div>
                                        <div class="featuredprojecthomepagepopupotherleftbin">
                                            <div class="featuredprojectnamelocation">
                                                CU Boulder Kittredge Loop
                                                <!-- end featuredprojectname -->
                                                - Boulder, Colo.
                                                <!-- end featuredprojectlocation -->
                                            </div>
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectdescriptionhomeother">The renovation of the Kittredge Complex included a complete interior renovation of four student residence...</div>
                                            <!-- end featuredprojectdescription -->
                                            <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="9" alt="" border="0">
                                            </div>
                                            <div class="featuredprojectlink">
                                                <a href="http://www.kiewit.com/projects/building/education/cu-boulder-kittredge-loop/">Read More</a>
                                            </div>
                                            <!-- end featuredprojectlink -->
                                        </div>
                                        <div class="clear"></div>
                                        <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="21" alt="" border="0">
                                        </div>
                                    </div>
                                    <!-- end featuredprojecthomepagepopupother -->








                                    <p style="text-align: right;"><a class="featuredprojectlink" title="Building" href="http://kiewit.com/markets/building/">View All</a>
                                    </p>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <!-- end contentbin -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="footeraccordions mobileonly">
                <div class="accordionheading" style="text-align:center; border-bottom:1px solid #ffce00 !important;">Quick Links</div>
                <div class="accordioninfo" style=" border-bottom:1px solid #ffce00;">

                    <ul class="ccm-manual-nav">

                        <li class="nav-selected nav-path-selected">
                            <a href="./Kiewit   Construction, Engineering and Mining Services_files/Kiewit   Construction, Engineering and Mining Services.html" class="nav-selected nav-path-selected">
			Home		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/" class="">
			Markets		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/about-us/" class="">
			About Us		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/projects/" class="">
			Projects		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/our-commitment/" class="">
			Our Commitment		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/media/" class="">
			Media		</a>
                        </li>

                    </ul>

                    <ul class="ccm-manual-nav">

                        <li class="">
                            <a href="http://kiewit.com/markets/building/" class="">
			Building		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/mining/" class="">
			Mining		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/oil-gas-chemical/" class="">
			Oil, Gas &amp; Chemical		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/power/" class="">
			Power		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/transportation/" class="">
			Transportation		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/markets/waterwastewater/" class="">
			Water/Wastewater		</a>
                        </li>

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="accordionheading" style="text-align:center; border-bottom:1px solid #ffce00 !important;">Featured Projects</div>
                <div class="accordioninfo" style=" border-bottom:1px solid #ffce00; text-align:center !important;">





                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Interior Footer" -->
                    <div class="featuredprojectinteriorfooter">
                        <div class="featuredprojectimg">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/b0021f28451a0a5cd886f7af2a664725_f1245.jpg" width="82" alt="Haynes No. 3 and No. 4 Generating Station" border="0">
                        </div>
                        <!-- end featuredprojectimg -->
                        <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="12" height="2" alt="" border="0">
                        </div>
                        <div class="featuredprojectinteriorfooterleftbin">
                            <div class="featuredprojectname">Haynes No. 3 and No. 4 Generating Station</div>
                            <!-- end featuredprojectname -->

                            <div class="featuredprojectlocation">Long Beach, Cal.</div>
                            <!-- end featuredprojectlocation -->

                            <div class="featuredprojectlink">
                                <a href="http://www.kiewit.com/projects/power/gas/haynes-no-3-and-no-4-generating-station-repowering/">Read More</a>
                            </div>
                            <!-- end featuredprojectlink -->
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- end featuredprojectinteriorfooter -->
                    <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="24" alt="" border="0">
                    </div>










                    <!-- ENTER MARKUP HERE FOR FIELD "Website Location" : CHOICE "Interior Footer" -->
                    <div class="featuredprojectinteriorfooter">
                        <div class="featuredprojectimg">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ee31abaed48282de698dffa44729b95d_f21.jpg" width="82" alt="TD Ameritrade Headquarters" border="0">
                        </div>
                        <!-- end featuredprojectimg -->
                        <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="12" height="2" alt="" border="0">
                        </div>
                        <div class="featuredprojectinteriorfooterleftbin">
                            <div class="featuredprojectname">TD Ameritrade Headquarters</div>
                            <!-- end featuredprojectname -->

                            <div class="featuredprojectlocation">Omaha, Neb.</div>
                            <!-- end featuredprojectlocation -->

                            <div class="featuredprojectlink">
                                <a href="http://www.kiewit.com/projects/building/commercial/td-ameritrade-headquarters/">Read More</a>
                            </div>
                            <!-- end featuredprojectlink -->
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- end featuredprojectinteriorfooter -->
                    <div id="spacerbin"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="24" alt="" border="0">
                    </div>






                </div>
                <div class="accordionheading" style="text-align:center; border-bottom:1px solid #ffce00 !important;">Contact Kiewit</div>
                <div class="accordioninfo" style="border-bottom:1px solid #ffce00; text-align:center !important; color:#fff;">
                    <h4 style="text-align: center;">Home Office :&nbsp;3555 Farnam St. Omaha, NE</h4>
                    <h4 style="text-align: center;">Questions :&nbsp;<a class="textlinkwhite" style="font-size: 14px; line-height: 1.5;" href="http://www.kiewit.com/info/contact-us/">Contact Us</a></h4>
                    <p style="text-align: center;"><a class="textlinkwhite" title="Locations" href="http://kiewit.com/!trash/locations-2/">Locations Page</a> &nbsp; | &nbsp;&nbsp;<a class="textlinkwhite" title="Careers" href="./Kiewit   Construction, Engineering and Mining Services_files/Kiewit   Construction, Engineering and Mining Services.html">Career Search</a>
                    </p>
                </div>
            </div>

            <div id="footerbinhome">
                <div id="columndivider" class="nomobile"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="30" height="2" alt="" border="0">
                </div>
                <div id="footerhomecopyrightbin" class="mgn-b--15">
                    <p>© 2014 Kiewit Corporation. All Rights Reserved.</p>
                </div>
                <div id="footerhomelinksbin" class="nophone notabletportrait">

                    <ul class="ccm-manual-nav">

                        <li class="">
                            <a href="http://kiewit.com/info/privacy-policy/" class="">
			Privacy Policy		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/info/terms-conditions/" class="">
			Terms &amp; Conditions		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/business-with-us/" class="">
			Business With Us</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/info/sitemap/" class="">
			Sitemap		</a>
                        </li>


                        <li class="">
                            <a href="http://kiewit.com/info/accessibility/" class="">
			Accessibility		</a>
                        </li>

                    </ul>
                    <div class="clear"></div>
                </div>
                <div id="footerhomesocialmediabin">
                    <div><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="1" height="6" alt="" border="0">
                    </div>
                    <div class="homepagesocial" id="socialmail">
                        <a href="http://kiewit.com/info/contact-us/"></a>
                    </div>
                    <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="13" height="2" alt="" border="0">
                    </div>
                    <div class="homepagesocial" id="socialfacebook">
                        <a href="http://www.facebook.com/kiewit?rf=114458608570884" target="_facebook"></a>
                    </div>
                    <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="13" height="2" alt="" border="0">
                    </div>
                    <div class="homepagesocial" id="socialtwitter">
                        <a href="http://twitter.com/kiewit" target="_twitter"></a>
                    </div>
                    <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="13" height="2" alt="" border="0">
                    </div>
                    <div class="homepagesocial" id="sociallinkedin">
                        <a href="http://www.linkedin.com/company/kiewit" target="_linkedin"></a>
                    </div>
                    <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="13" height="2" alt="" border="0">
                    </div>
                    <div class="homepagesocial" id="socialyoutube">
                        <a href="http://www.youtube.com/user/KiewitCorporation" target="youtube"></a>
                    </div>
                    <div id="columndivider"><img src="<?php echo get_template_directory_uri(); ?>/images/fill.gif" width="17" height="2" alt="" border="0">
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- end maxcontainerbin -->
        <div id="button" class="mobileonly graphicmo2"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_icon.png" width="22" height="22" alt="Menu" title="Menu" border="0">
        </div>
    </div>
    <spots-wrapper></spots-wrapper>

</body>
</html>
