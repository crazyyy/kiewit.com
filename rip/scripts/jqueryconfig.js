$(document).ready(function() { 
	
	//var rand = Math.floor(Math.random() * $('#header-image-interior img').length);
	//var hdrimg = $("#header-image-interior img").eq(rand).attr("src");
	var hdrimg = $("#header-image-interior img").attr("src");
	$("#header-image-interior").css("background-image", "url(" + hdrimg + ")");
	$("#header-image-interior").css("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + hdrimg + "', sizingMethod='scale')");
	$("#header-image-interior").css("-ms-filter", "\"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + hdrimg + "',sizingMethod='scale')\";");
	
	$("#header-image").css("background-size", "cover");
	$("#header-image-interior").css("background-size", "cover");
	
	//ACCORDION TOGGLE
	$(".accordionheading").click(function () {
        //alert("yeah");
		$(this).next('.accordioninfo').slideToggle("fast");
    });	
	
	//MOBILE NAV SIDE TOGGLE
	$('#button').toggle( 
    function() {
        $('#right').animate({ left: -200 }, 'slow');
    }, 
    function() {
        $('#right').animate({ left: 0 }, 'slow');
    });
	
	// TTT BUTTON FADE
	$(window).scroll(function() {
		if ($(this).scrollTop()) {			
			var w = Math.round($(document).width()/2 + 480); 
			$('#toTop').css('left', w + 'px');
			$('#toTop:hidden').stop(true, true).fadeIn();
		} else {
			$('#toTop').stop(true, true).fadeOut();
		}
	});	
	
	// TTT SCROLL FUNCTION
	$('a[href=#top]').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });
	
	// SEARCH FEATURE, SWAP DEFAULT TEXT
  	$('.prefillvalue').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
				this.value = default_value;
			}
		});
    });
    // SEARCH FEATURE, OPEN/CLOSE
    $(".opensearchbtn").click(function () {
        $("#searchfieldbin").slideDown("fast");
        $(".opensearchbtn").css("display", "none");
    });
    $(".closesearchbtn").click(function () {
        $("#searchfieldbin").slideUp("fast");
        $(".opensearchbtn").css("display", "block");
    });
	
	// FLYOUT MENU TRIGGERS
	$('#headerbin ul.nav li').mouseover(function(e) {
        $(this).find('ul:first').css('display','block');
    });

	$('#headerbin ul.nav li').mouseout(function(e) {
        $(this).find('ul:first').css('display','none');
    });
	
	// SWAP DEFAULT TEXT
  	$('.formfieldswap').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.id == "uPassword" || this.id == "uPasswordConfirm") {
				this.type = 'password';
			}
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
				this.value = default_value;
				if(this.id == "uPassword" || this.id == "uPasswordConfirm" ) {
					this.type = 'text';
				}
			}			
		});
    });
	
});
// JUMP TO FUNCTION FOR MOBILE NAVIGATION
$(function (){  
	$("#mobilegoto").change(function(e) {
		window.location.href = $(this).val();
	});
});